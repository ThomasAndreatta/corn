#!/usr/bin/python3

import time,os,sys,argparse
from lazyt import *
from func import *
from datetime import datetime
from hurry.filesize import size
from argparse import RawTextHelpFormatter
from columnar import columnar
import glob
total_size=0
e = enc()

show_key = False
options =["Exit","Encrypt","Decrypt","Load a new key","Generate a new key","Show/Hide current key"]
max_choice =len(options)# debugpurpose
rows=2
key=None
base_banner=lazyt.create_banner(options,rows)
banner=base_banner
exitoption=['e','exit','q']


def dirtyjob(path_list,base_folder,mode):


    total_size=0
    for x in path_list:
        total_size+=os.path.getsize(x)
    if(mode):
        print("======== ENCRYPTING ========")
    else:
        print("======== DECRYPTING ========")
    print("Base folder:\t{}\nNumber of file:\t{}\nEstimated size:\t{}\n".format(base_folder,len(path_list),size(total_size)))
    done_size=0
    num_file=len(path_list)
    done_file=0
    start_time = datetime.now()
    message="It's dirty down here"
    for x in path_list:
        done_size+=os.path.getsize(x)
        try:
            if(mode):
                e.encrypt(x)
            else:
                e.decrypt(x)
        except Exception as a:
            done_size-=os.path.getsize(x)
            total_size-=os.path.getsize(x)
            pass


        done_file+=1
        print("{}  [{}/{}] [{}/{}]".format(message,done_file,num_file,size(done_size),size(total_size)),end = "\r")
        #tprint.info("{}  [{}/{}] [{}/{}]".format(message,done_file,num_file,size(done_size),size(total_size)),end_el = "\r")


    end_time = datetime.now()
    print("\n")
    if(mode):
        print("======== ENCRYPTED ========")
    else:
        print("======== DECRYPTED ========")
    print("Total time: {}".format(end_time - start_time))
    print("Total file: ",done_file)
    print("Total size: ", size(total_size))
    input("Press any key to proceed...")
    lazyt.clear_console()

def main():
    global e,show_key,banner,total_size
    last_operation='None'
    new_key=""
    while(True):
        lazyt.clear_console()

        c = num_choice(max_choice,banner="Last operation: {}".format(last_operation)+'\n\n'+banner)
        total_size=0
        if c == 0:#done
            print("Bye!\n")
            exit(0)
        elif c == 1:#encrypt
            if(key != None):
                print("\t\t======== ENCRYPTION MODE ========")
                path_list,base_folder = selection()
                if(base_folder !=''):
                    last_operation="Encryption of [{}]".format(base_folder)
                    if(len(path_list)>0):
                        dirtyjob(path_list,base_folder,True)
            else:
                print("No key has been loaded!")
        elif c == 2:#decrypt
            if(key != None):
                print("\t\t======== DECRYPTION MODE========")
                path_list,base_folder = selection()
                if(base_folder !=''):
                    last_operation="Decryption of [{}]".format(base_folder)
                    if(len(path_list)>0):
                        dirtyjob(path_list,base_folder,False)
            else:
                print("No key has been loaded!\n")
        elif c ==3:##load key DONE
            load()
            last_operation="Loaded the key [{}]".format(key)
        elif c== 4:#generate key DONE
            n = input("keyfile name> ")
            if not n.endswith('.key'):
                n +='.key'
            success = e.generate_key(output_name=n)
            if(success):
                last_operation="Generated the key [{}]".format(n)
                use = lazyt.yon("Load the new key?")
                if(use):
                    e.load_key(n)
                    updatebanner()

        elif c == 5:#show/hide key DONE
            show_key = not show_key
            if(show_key):
                updatebanner()
            else:
                banner=base_banner




def selection():
    folder_options=["[ONLY]Current folder","[RECURSIVE]Current folder","[ONLY]Different folder","[RECURSIVE]Different folder"]
    folder_banner = lazyt.create_banner(folder_options,cols=2)+'\t\t\t[4] ABORT'
    file_list=[]
    media_options=["[ONLY]Pictures","[ONLY]Video","[ONLY]Different extension","[ALL]All multimedia"]
    pic_ext=["jpeg","jpg","png","gif","tiff","psd","raw"]
    video_ext=["webm", "mkv", "flv", "vob", "ogv", "ogg", "drc", "gif", "avi", "mov", "qt", "wmv", "amv", "mp4", "m4p", "m4v", "mpg", "mp2", "mpeg", "mpe", "mpv", "svi", "m4v", "flv", "f4v", "f4p", "f4a", "f4b"]
    media_banner = '\n'+lazyt.create_banner(media_options,cols=2)+'\t\t\t[4]Everything'
    media_banner+="\n\t\t\t[5] ABORT"
    while True:
        folder_choice = num_choice(max=5,banner=folder_banner)
        folder_target= ''
        folder_recursive= False

        if folder_choice == 0:
            folder_target=os.getcwd()+'/'
        if folder_choice == 1:
            folder_target=os.getcwd()+'/'
            folder_recursive=True
        if folder_choice == 2:
            folder_tmp=input("Target folder> ")
            if(lazyt.yon("Are you sure?\n[{}]".format(folder_tmp))):
                folder_target=folder_tmp
            else:
                continue
        if folder_choice==3:
            folder_tmp=input("Target folder> ")
            if(lazyt.yon("Are you sure?\n[{}]".format(folder_tmp))):
                folder_target=folder_tmp
                folder_recursive=True
            else:
                continue
        if folder_choice ==4:
            return file_list,''


        while True:
            media_choice = num_choice(max=6,banner=media_banner)
            if media_choice == 0:#picture
                file_list= find_file(folder_target,folder_recursive,ext=pic_ext)

            if media_choice == 1:#video
                file_list= find_file(folder_target,folder_recursive,ext=video_ext)
            if media_choice == 2:#personalized
                correct=True
                while (correct):
                    ext=input("Extensions[comma separated]> ")
                    personal_ext=ext.replace('.','').replace(' ','').rstrip().split(',')
                    print(personal_ext)
                    if(lazyt.yon("Confirm?")):
                        correct=False

                file_list= find_file(folder_target,folder_recursive,ext=personal_ext)

            if media_choice==3:#all multimedia
                file_list= find_file(folder_target,folder_recursive,ext=video_ext+pic_ext)
            if media_choice==4:#all file
                file_list= find_file(folder_target,folder_recursive)
            if media_choice ==5:
                break

            return file_list,folder_target

def find_file(folder_target,folder_recursive,ext=['']):
    file_list=[]
    if(folder_recursive):
        file_list=[f for f in (glob.glob(folder_target + '/**/*', recursive=True)) if os.path.isfile(f) and f.lower().endswith(tuple(ext))]
    else:
        file_list = [f for f in os.listdir(folder_target) if os.path.isfile(folder_target+'/'+f) and f.lower().endswith(tuple(ext))]
    return file_list

def updatebanner():
    global banner,show_key
    banner=base_banner+'\nCurrent key file: '+str(key)
    if(show_key):
        k = e.get_key()
        if(k=='.'):
            show_key=False
            print("No key has been loaded!")
        else:
            banner+="\nCurrent key string: "+k

def load():#load new key
    global e,banner,key
    while True:

        print("Key file name/path [q for abort]")

        q = input('>')
        if(q.lower() =='q'):
            print("Loading interrupted!\n")
            break

        if not q.endswith('.key'):
            q +='.key'


        if(os.path.isfile(q)):
            e.load_key(q)
            key = q[:-4]


            updatebanner()
            break

        else:
            print("That's not a valid file!\n")

def num_choice(max,min=-1,banner='',error_message='Not a valid option!'):#choose what he needs
    q=''
    while True:
        print(banner)
        try:
            q = input('>')
            q=int(q)
            print("\n")
            if(q > min and q <= max):
                break
            else:
                print(error_message)
        except:
            print(error_message)
    return q

def menu():#argparse menu
    parser = argparse.ArgumentParser(description='Crypt shit.',formatter_class=RawTextHelpFormatter)
    group = parser.add_mutually_exclusive_group()

    parser.add_argument('-k','--keyfile',
                        help='FULL key_file name (default is key.key)\n\n',
                        type=str,
                        required=False)
    group.add_argument('-e','--encrypt',
                        help='file_name of the file to encrypt(empty for additional menu)\n\n',
                        action='store', nargs='?',type=str,
                        required=False,)
    group.add_argument('-d','--decrypt',
                        help='file_name of the file to decrypt(empty for additional menu)\n\n',
                        action='store',nargs='?',type=str,
                        required=False)

    return parser.parse_args()

def my_except_hook(exctype, value, traceback):#gloabl handler for unmatching pattern key-file signature
    import cryptography
    if exctype == cryptography.fernet.InvalidToken:
        print ("Bad signature!\nProbably wrong key, file not crypyted or damaged!\n")
    else:
        #print(value)
        sys.__excepthook__(exctype, value, traceback)
sys.excepthook = my_except_hook

def check():#load the key and check if only one shot is needed
    global e,key
    if(args.keyfile != None):
        if not args.keyfile.endswith('.key'):
            e.load_key(key_name=args.keyfile+'.key')
            key = args.keyfile
        else:
            e.load_key(key_name=args.keyfile)
            key = args.keyfile[:-4]
    else:
        if os.path.isfile('key.key'):
            e.load_key()
        else:
            print("No default key file [key.key].\nSpecify a [-k KEYFILE] parameters.")
            exit(1)
    if(args.encrypt != None):
        e.encrypt(args.encrypt)
        exit(0)
    elif(args.decrypt != None):
        e.decrypt(args.decrypt)
        exit(0)

def debug():
    import time

    text = '\rgood-bye'
    for i in range(10,14):
        print (i,end = "\r")
        time.sleep(1)
    print (' ')

is_debug=False
if __name__ == "__main__":

    if(not is_debug):
        args = menu()
        if os.path.isfile('key.key'):
            e.load_key()
            key='key'

        if len(sys.argv) > 1:
            check()

        updatebanner()


        start_time = datetime.now()
        main()
        end_time = datetime.now()
        print("Total time: {}".format(end_time - start_time))
        print("Total size: ", size(total_size))
    else:
        debug()
