# Corn
## Purpose
This tool will provide an easy and - when coded in c - fast way to encrypt your file using AES algorithm.
Born from the necessity of hiding big archive of files without having the time and the will of literally hide them somewhere and coding something it's always fun so here you go.

## File Content
* func.py:
    * encryption/decryption function

* corn.py:
    * This is the one that get the job done

-----

## PYTHON COMPLETED
* init the project lol
* single shot encrypt/decrypt
* argument parsing
* super cool and automatic menu banner
* key loading and generation
* enc/dec of full folder
-----
## Working on (C)
* encryption and decryption of a file library

-----


##### NOTE
* Once done in python i'll probably re-code it in C for efficiency and speed
