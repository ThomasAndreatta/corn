from cryptography.fernet import Fernet
from lazyt import *
class enc:
    key = ''
    f = ''

    def encrypt(self, filename):
        with open(filename, "rb") as file:
            file_data = file.read()
        encrypted_data = self.f.encrypt(file_data)
        with open(filename, "wb") as file:
            file.write(encrypted_data)

    def decrypt(self, filename):
        with open(filename, "rb") as file:
            encrypted_data = file.read()
        decrypted_data = self.f.decrypt(encrypted_data)
        with open(filename, "wb") as file:
            file.write(decrypted_data)

    def generate_key(self,output_name="key.key"):
        import os

        if(os.path.isfile(output_name)):
            c = lazyt.yon("The file [{}] already exist!\nOverride it?".format(output_name))
            if(c):
                pass
            else:
                return False


        key = Fernet.generate_key()
        with open(output_name, "wb") as key_file:
            key_file.write(key)

        return True

    def load_key(self,key_name="key.key"):
        self.key = open(key_name, "rb").read()
        self.f = Fernet(self.key)


    #=========================================
    #============== DEBUG ====================
    #=========================================
    def get_key(self):
        q=""
        try:
            q=self.key.decode("utf-8")
            return q
        except Exception as e:
            return '.'
